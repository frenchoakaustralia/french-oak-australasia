French Oak Australasia previously known as French Oak Australia, Salvage Group and The Salvage Company offers dedication and an extraordinary experience, knowledge, and passion. We travel across the globe looking for the best antique woods and French Oak Australasia architectural elements to be given new life in contemporary architectural projects and interior fit-Â­outs.

We are here to assist you in bringing your project to life. If there is anything we can help you with, get in touch today.

Website: http://www.frenchoakausnz.com.au/
